# git-issue

A git helper for [drupal issue forks][drupalorg-issue-forks].

Currently, it is a set of git aliases.

To set it up, just [copy the aliases][aliases] into your git configuration.
E.g. at `.gitconfig` or `.config/git/config`.

[aliases]: https://git.drupalcode.org/project/git_issue/-/blob/main/.gitconfig
[drupalorg-issue-forks]: https://www.drupal.org/docs/develop/git/using-gitlab-to-contribute-to-drupal/introduction
